#!/bin/bash

INPUT="$1"
NAME="${INPUT%%.*}"

# Treshold for black detection, can be tweaked (lower number = more dark tolerance)
THRESH="9800"

mkdir -p /tmp/"${NAME}"

# Get total length of the video
FULLTIME=`ffmpeg -i "$INPUT" 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//`;

# Total Picture number
THUMBN="20"

# Length of the video in seconds, rounded up/down
DURATION=`echo "$FULLTIME" | awk -F: '{ print ($1 * 3600) + ($2 * 60) + $3 }' | awk '{print int($1+0.5)}'`


# ffmpeg

# starting the "loop" depending on max thumbnails


for i in $(eval echo "{01..$THUMBN}")
do
    # some math to get an evenly spread start time for each image
    TIME=`echo "($i - 0.2) * $DURATION / $THUMBN" | bc | awk '{print int($1+0.5)}'`
    TRIES="1"

    # go to the specified start time and take one image
    echo "Creating image $i / $THUMBN"
    ffmpeg -hide_banner -loglevel 0 -ss $TIME -i "$INPUT" -vf select="eq(pict_type\,I)" -vframes 1 -y -f image2pipe -vcodec png - | convert - -sigmoidal-contrast 2,50% /tmp/${NAME}/thumb$i.png 2>/dev/null

    SIZE=$(stat -c%s /tmp/"${NAME}"/thumb$i.png)

    convert /tmp/"${NAME}"/thumb$i.png -alpha off -fill black -colorize 100% /tmp/image_black.png 2>/dev/null
    convert /tmp/"${NAME}"/thumb$i.png -alpha off -fill white -colorize 100% /tmp/image_white.png 2>/dev/null

    WVALUE=`compare -metric RMSE /tmp/"${NAME}"/thumb$i.png /tmp/image_white.png /tmp/image_out2.png 2>&1`
    WHITE=`echo "$WVALUE" | awk '{print $1}' | awk '{print int($1+0.5)}'`

    BLVALUE=`compare -metric RMSE /tmp/"${NAME}"/thumb$i.png /tmp/image_black.png /tmp/image_out.png 2>&1`
    BLACK=`echo "$BLVALUE" | awk '{print $1}' | awk '{print int($1+0.5)}'`

    # Check if image is too dark or too white, when it is, take a new one 5 seconds(?) later, max 10 tries, after that, give up.
    while [ "$SIZE" -lt "300" ] || [ "$BLACK" -lt "$THRESH" ] || [ "$WHITE" -lt "$THRESH" ] && [ "$TRIES" -lt "10" ]
    do

        echo "Taking a new Picture."

        TIME=$(expr $TIME + 5)
        TRIES=$(expr $TRIES + 1)

        ffmpeg -hide_banner -loglevel 0 -ss $TIME -i "$INPUT" -vf select="eq(pict_type\,I)" -vframes 1 -y -f image2pipe -vcodec png - | convert - -quiet -sigmoidal-contrast 2,50% /tmp/"${NAME}"/thumb$i.png 2>/dev/null

        SIZE=$(stat -c%s /tmp/"${NAME}"/thumb$i.png)

        convert /tmp/"${NAME}"/thumb$i.png -alpha off -fill black -colorize 100% /tmp/image_black.png 2>/dev/null
        convert /tmp/"${NAME}"/thumb$i.png -alpha off -fill white -colorize 100% /tmp/image_white.png 2>/dev/null

        WVALUE=`compare -metric RMSE /tmp/"${NAME}"/thumb$i.png /tmp/image_white.png /tmp/image_out2.png 2>&1`
        WHITE=`echo "$WVALUE" | awk '{print $1}' | awk '{print int($1+0.5)}'`

        BLVALUE=`compare -metric RMSE /tmp/"${NAME}"/thumb$i.png /tmp/image_black.png /tmp/image_out.png 2>&1`
        BLACK=`echo "$BLVALUE" | awk '{print $1}' | awk '{print int($1+0.5)}'`

    done


done

# Imagemagick stuff, for creating the landscape
montage /tmp/"${NAME}"/thumb* \
    -background black \
    -tile 5x \
    -mode Concatenate \
    -trim \
    +repage \
    /tmp/tmp.mpc

convert /tmp/tmp.mpc \
    -write "${INPUT}".png \
    -colorspace LAB \
    -filter Lanczos \
    -distort resize x1080 \
    -unsharp 0x0.75+0.75+0.008 \
    -gravity north-west \
    -crop 1920x1080+0+0 \
    +repage \
    -quality 100% \
    landscape.jpg


rm -fr /tmp/"${NAME}"
rm /tmp/tmp.cache /tmp/tmp.mpc
echo "Unscaled File: "${INPUT}".png created."
echo "1920x1080 version, landscape.jpg, for XBMC/KODIcreated."
