#!/bin/bash

INPUT="$1"
NAME="${INPUT%%.*}"

THRESH="9800"

mkdir -p extrathumbs

# Get total length of the video
FULLTIME=`ffmpeg -i "$INPUT" 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//`;

THUMBN="10"

# Length of the video in seconds, rounded up/down
DURATION=`echo "$FULLTIME" | awk -F: '{ print ($1 * 3600) + ($2 * 60) + $3 }' | awk '{print int($1+0.5)}'`


# ffmpeg

# starting the "loop" depending on max thumbnails


for i in $(eval echo "{1..$THUMBN}")
do
    # some math to get an evenly spread start time for each image
    TIME=`echo "($i - 0.2) * $DURATION / $THUMBN" | bc | awk '{print int($1+0.5)}'`

    TRIES="0"
    # go to the specified start time and take one image
    echo "Creating image $i / $THUMBN"
    ffmpeg -hide_banner -loglevel 0 -ss $TIME -i "$INPUT" -vf select="eq(pict_type\,I)" -vframes 1 -y -f image2pipe -vcodec png - | convert - -sigmoidal-contrast 2,50% -fuzz 10% -bordercolor black -trim +repage extrathumbs/thumb$i.png 2>/dev/null

    SIZE=$(stat -c%s extrathumbs/thumb$i.png)

    convert extrathumbs/thumb$i.png -alpha off -fill black -colorize 100% /tmp/image_black.png 2>/dev/null
    convert extrathumbs/thumb$i.png -alpha off -fill white -colorize 100% /tmp/image_white.png 2>/dev/null


    WVALUE=`compare -metric RMSE extrathumbs/thumb$i.png /tmp/image_white.png /tmp/image_out2.png 2>&1`
    WHITE=`echo "$WVALUE" | awk '{print $1}' | awk '{print int($1+0.5)}'`

    BLVALUE=`compare -metric RMSE extrathumbs/thumb$i.png /tmp/image_black.png /tmp/image_out.png 2>&1`
    BLACK=`echo "$BLVALUE" | awk '{print $1}' | awk '{print int($1+0.5)}'`


    while [ "$SIZE" -lt "300" ] || [ "$BLACK" -lt "$THRESH" ] || [ "$WHITE" -lt "$THRESH" ] && [ "$TRIES" -lt "10" ]
    do

        echo "Taking a new Picture."

        TIME=$(expr $TIME + 5)
        TRIES=$(expr $TRIES + 1)

        ffmpeg -hide_banner -loglevel 0 -ss $TIME -i "$INPUT" -vf select="eq(pict_type\,I)" -vframes 1 -y -f image2pipe -vcodec png - | convert - -quiet -sigmoidal-contrast 2,50% -fuzz 10% -bordercolor black -trim +repage extrathumbs/thumb$i.png 2>/dev/null

    SIZE=$(stat -c%s extrathumbs/thumb$i.png)

    convert extrathumbs/thumb$i.png -alpha off -fill black -colorize 100% /tmp/image_black.png 2>/dev/null
    convert extrathumbs/thumb$i.png -alpha off -fill white -colorize 100% /tmp/image_white.png 2>/dev/null


    WVALUE=`compare -metric RMSE extrathumbs/thumb$i.png /tmp/image_white.png /tmp/image_out2.png 2>&1`
    WHITE=`echo "$WVALUE" | awk '{print $1}' | awk '{print int($1+0.5)}'`

    BLVALUE=`compare -metric RMSE extrathumbs/thumb$i.png /tmp/image_black.png /tmp/image_out.png 2>&1`
    BLACK=`echo "$BLVALUE" | awk '{print $1}' | awk '{print int($1+0.5)}'`

    done


done

# convert to jpg garbage
mogrify -format jpg -quality 100% extrathumbs/*.png
rm extrathumbs/*.png
