# Thumbnail Creator

Little personal shell script to create a Scene overview, which I use in XBMC/KODI as the Landscape.jpg file.

It basically does this:

1. Get length of Movie
2. Convert length to seconds
3. Run ffmpeg xx times, which can be changed, and create a Screenshot everytime.
4. Check every image with imagemagick, if it's too dark or too bright, as in "black" and "white", take a new Screenshot later (max 10 tries).
5. Save a full size Overview.
6. Create Landscape.jpg for XBMC/KODI use.


## Prerequisites

* ffmpeg
* Imagemagick


## Usage

`./landscape.sh VIDEO.XXX`


## Issues

* Probably a bunch.

## Sample Images

- Created single images.
![Single images](https://github.com/agony/tn/raw/master/images/01.png)

- Landscape.jpg
![Scaled Landscape](https://github.com/agony/tn/raw/master/images/03.jpg)
